#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` file.dart"
  exit 5
fi

FILE=$(readlink -f $0)
FPATH=$(dirname $FILE)/web
OPATH=$FPATH/../out

rm -rf $OPATH
rm -rf $FPATH/out
mkdir -p $OPATH
cp -fr $FPATH/* $OPATH
rm -rf $OPATH/packages
cp -fLr $FPATH/packages $OPATH
dart2js $FPATH/$1 -o$OPATH/$1.js
mv $OPATH $FPATH
