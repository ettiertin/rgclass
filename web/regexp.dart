library regexp;

import 'dart:html';
import 'dart:math';
import 'Exercise.dart';

part 'exercises.dart';

InputElement regexp;
ParagraphElement subject;
UListElement toMatch;
DivElement result;
DivElement successContainer;
UListElement quicktips;
int nbExercise = 0;
ButtonElement nextex;
DivElement raptor;

void main()
{
  subject = query("#subject");
  regexp = query("#regexp");
  toMatch = query("#toMatch");
  result = query("#result");
  quicktips = query("#quicktips");
  raptor = query("#raptor_container");
  successContainer = query("#success_container");
  nextex = query("#nextex");

  // display raptor
  ImageElement img_raptor = new ImageElement();
  img_raptor.src = "img/raptor_small.jpg";
  raptor.children.add(img_raptor);

  // display first exercise
  displayNextExercise();

  // first hide the next button
  hidesuccess(true);

  // listen to events on button or regexp
  regexp.onChange.listen(regexp_and_display);
  nextex.onClick.listen(clear_and_nextex);
}

void clear_all() {
  // clear pretty much everything
  toMatch.children.clear();
  quicktips.children.clear();
  subject.children.clear();
  //regexp.children.clear();
  result.children.clear();
  regexp.placeholder = "Write your regexp here";
  regexp.value = "";

}

void handle_end() {
  clear_all();
  regexp.hidden = true;
  subject.text = "Congratulations! You made it 'til the end!";
}

void clear_and_nextex(Event e) {
  clear_all();
  nbExercise++;

  if (nbExercise >= exercises.length) {
    handle_end();
  } else {
    displayNextExercise();
  }
  hidesuccess(true);
}

void displayNextExercise()
{
  Exercise e = exercises[nbExercise];
  subject.text = e.subject;

  LIElement tip = new LIElement();
  tip.text = e.quickTip;
  quicktips.children.add(tip);

  for (var match in e.toMatch) {
    LIElement newToMatch = new LIElement();
    newToMatch.text = match;
    toMatch.children.add(newToMatch);
  }
}

LIElement getFromMatch(Match m, String cssClass) {
  LIElement newRes = new LIElement();

  if (m.start > 0) {
    SpanElement before = new SpanElement();
    before.text = m.str.substring(0, m.start);
    newRes.children.add(before);
  }

  if (m.start < m.end) {
    SpanElement match = new SpanElement();
    match.text = m.str.substring(m.start, m.end);
    match.classes.add(cssClass);
    newRes.children.add(match);
  }

  if (m.end < m.str.length) {
    SpanElement after = new SpanElement();
    after.text = m.str.substring(m.end, m.str.length);
    newRes.children.add(after);
  }

  return newRes;
}

/*
 * There is currently a bug with js or dart2js (?) that make counting match for certain regex
 * like .* infinite (first match everything and all the one following are equals)...
 * This is quite ugly but it will work...
 * Anyway we shouldn't have any exercise that want to match > 50 sample I think...
 */
int countMatch(Iterable<Match> iterable) {
  Iterator<Match> it = iterable.iterator;
  Match old = null;
  int i=0;
  for (i=0; it.moveNext() && i < 50; i++) {
    if (old != null && it.current.start == old.start && it.current.end == old.end) {
      i--;
      break;
    }
    old = it.current;
  }
  return i;
}

void hidesuccess(bool show){
    successContainer.hidden = show;
}

void regexp_and_display(Event e)
{
  Exercise e = exercises[nbExercise];

  bool success = true;

  result.children.clear();

  RegExp exp = new RegExp(regexp.value);

  for (int i=0; i < e.toMatch.length ; i++) {
    String str = e.toMatch[i];
    ParagraphElement p = new ParagraphElement();
    p.text = str;
    result.children.add(p);

    UListElement ul = new UListElement();
    Iterable<Match> matches = exp.allMatches(str);
    int countMatches = countMatch(matches);
    if (matches.isEmpty) { // NO MATCH FOUND
      if (e.expectedOutput[i].length <= 0) {
        LIElement newRes = new LIElement();
        newRes.text += "0 matches [Success] \n";// NO MATCH EXPECTED
        newRes.classes.add("success");
        ul.children.add(newRes);
      } else {
        LIElement newRes = new LIElement();
        newRes.text += "0 matches [Fail] \n"; // MATCHES WERE EXPECTED
        newRes.classes.add("fail");
        ul.children.add(newRes);
        success = false;
      }
    } else { // MATCHES FOUND
      if (e.expectedOutput[i].length <= 0) { // NO MATCH EXPECTED
        for (int j=0 ; j < countMatches ; j++) {
          ul.children.add(getFromMatch(matches.elementAt(j), "fail"));
        }
        success = false;
      } else { // MATCHES WERE EXPECTED
          int last =0;
          if (countMatches > e.expectedOutput[i].length)
            last = countMatches;
          else
            last = e.expectedOutput[i].length;

          for (int j=0 ; j < last ; j++) {
            if (j < countMatches && j < e.expectedOutput[i].length) {
              LIElement newRes;
              if (matches.elementAt(j).group(0) == e.expectedOutput[i][j]) {
                newRes = getFromMatch(matches.elementAt(j), "success");
              } else {
                newRes = getFromMatch(matches.elementAt(j), "fail");
                success = false;
              }
              ul.children.add(newRes);
            } else if (j > e.expectedOutput[i].length && j < countMatches) {
              LIElement newRes;
              newRes = getFromMatch(matches.elementAt(j), "fail");
              success = false;
              ul.children.add(newRes);
            } else if (j > e.expectedOutput[i].length && j > countMatches) {
              LIElement newRes;
              newRes.text = "Missing 1 match: " +  e.expectedOutput[i][j];
              newRes.classes.add("fail");
              success = false;
              ul.children.add(newRes);
            }
          }
      }
    }
    result.children.add(ul);
  }
  // handle success like a boss
  handle_success(success);
}

void handle_success(bool ok){
  if (ok == true){
    hidesuccess(false);
  } else {
    hidesuccess(true);
  }
}


