part of regexp;

List<Exercise> exercises = [
new Exercise(
  1,
  "First lesson: any string made of letters or numbers matches itself. Try to match 'toto' in these samples:",
  "abc123 matches abc123",
  ["Lui c'est toto.", "Moi je m'appelle titi.", "titi et toto c'est pas pareil.", "Kamoulox."],
  [["toto"], [], ["toto"], []],
  "toto"
),

new Exercise(
  2,
  "Did you know that ^ is used to match the beginning of a line? Try to match Toto in these samples, but only when located at the beginning of a line!",
  "^A matches the letter A at the beginning of a line",
  ["J'ai vu Toto ce matin.", "Toto ne m'a pas vu", "Probablement parce que Toto est aveugle", "C'est vraiment méchant."],
  [[], ["Toto"], [], []],
  "^Toto"
),

new Exercise(
  3,
  "Did you know that \$ is used to match the end of a line? Try to match Toto in these samples, but only when located at the end of a line!",
  "A\$ matches the letter A at the end of a line",
  ["Where is Toto?", "I'm looking for Toto", "He looks a lot like Toti", "Maybe I'll have a beer waiting for him."],
  [[], ["Toto"], [], []],
  "\$Toto"
),

new Exercise(
  4,
  "Some characters are special for regular expressions. The single dot '.' is one of them. It matches everything (except for the end-of-line character). Try to only match strings with exactly one character",
  "'.' matches any character",
  ["","aa", "4", "This one should not be matched obviously", "M"],
  [[],[], ["4"], [], ["M"]],
  "^.\$"
),

new Exercise(
  5,
  "Square brackets are used to match a specific set of character. Try to match every word starting with a 'T' followed by two vowels.",
  "[a-z] means one letter from a to z, [0123] means either a 0, or a 1, or a 2, or a 3.",
  ["Tea is good for your health", "Toto made some tea", "Where did you put my tee?", "Tee are useful when playing golf"],
  [["Tea"], [], [], ["Tee"]],
  "T[aeiou][aeiou]"
),

];