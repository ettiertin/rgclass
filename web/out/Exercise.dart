library Exercise;

class Exercise {
  final num exNumber;
  final String subject;
  final String quickTip;
  final List<String> toMatch;
  final List<List<String>> expectedOutput;
  final String expectedRegexp;

  const Exercise(
      this.exNumber,
      this.subject,
      this.quickTip,
      this.toMatch,
      this.expectedOutput,
      this.expectedRegexp
      );

}